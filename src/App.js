import './App.css';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Home from "./pages/Home/Home";
import Product from "./pages/Products/Product";
import Login from "./pages/System/Login";
import Register from "./pages/System/Register";
import Create from "./pages/System/CreateProduct";
import Update from "./pages/System/UpdateProduct";
import Setting from "./pages/Setting/Setting";
import DetailProduct from "./pages/Products/DetailProduct";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />
  },
  {
    path: "/products",
    element: <Product />
  },
  {
    path: "/login",
    element: <Login />
  },
  {
    path: "/register",
    element: <Register />
  },
  {
    path: "/create",
    element: <Create />
  },
  {
    path: "/update/:id",
    element: <Update />
  },
  {
    path: "/setting",
    element: <Setting />
  },
  {
    path: "/detail/:id",
    element: <DetailProduct />
  }
]);

function App() {
  return (
    <div className='App'>
      <RouterProvider router = {router} />
    </div>
  );
}

export default App;
