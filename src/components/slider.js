import React from 'react'
import SimpleImageSlider from "react-simple-image-slider";
import slider1 from '../assets/slider1.jpg';
import slider2 from '../assets/slider2.jpg';
import slider3 from '../assets/slider3.jpg';
import slider4 from '../assets/slider4.jpg';

const images = [
    { url: slider1 },
    { url: slider2 },
    { url: slider3 },
    { url: slider4 },
];

function slider() {

    return (
        
        <div className='listslider'>
            <div>
            <SimpleImageSlider
                width={1515}
                height={604}
                images={images}
                showBullets={true}
                showNavs={true}
            />
            </div>
        </div>
        
    )
}

export default slider