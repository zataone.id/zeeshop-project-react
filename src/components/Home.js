import { useEffect, useState } from 'react';
import Slider from './slider';
import axios from "axios";
import { Link } from 'react-router-dom';

function Home() {
    const [products, setProduct] = useState([]);

    const fetchProduct = async () => {
    try {
        const response = await axios.get(
            "https://arhandev.maisyah.id/api/final/products"
        );
        setProduct(response.data.data);
        } catch (error) {
        console.log(error);
        }
    };

    useEffect(() => {
        fetchProduct();
    }, []);

    return (
        <><Slider />
        <div className='home'>
            
            {/* Card */}
            <div className="listData">
                <div class="HeaderList">
                    <h1 class="nameHeader">Catalog Product</h1>
                    <a href="/products">
                        <button class="px-4 py-2 ">See More</button>
                    </a>
                </div>
                <div className="containerList">
                    <div className="dataProduct">
                        {products.slice(0, 4).map((produk) => [
                            <div className="card">
                                <Link to={`/detail/${produk.id}`}>
                                <div className="card2">
                                    <img src={produk.image_url} alt="product" className="product" />
                                    <div className="List-data">
                                        <h1>{produk.nama.substring(0, 20)}</h1>
                                        {produk.is_diskon === 1 ? <div className='diskon'> <p className="harga-asli">Rp.{produk.harga_display}</p> <p className="harga-diskon">Rp.{produk.harga_diskon_display}</p></div>
                                            : produk.is_diskon === 0 ? <p className='hargaAsli'>Rp.{produk.harga_display}</p>
                                                : "gagal"}
                                        {/* <p>Rp.{produk.harga}</p> */}
                                        <p className="stock">Stock {produk.stock}</p>
                                    </div>
                                </div>
                                </Link>
                            </div>,
                        ])}
                    </div>
                </div>
            </div>
        </div></>
    )
}

export default Home