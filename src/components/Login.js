import axios from 'axios';
import {useState} from 'react';
import { useNavigate } from 'react-router-dom';


function Login() {
    const [input, setInput] = useState({
        email: '',
        password:'',
    });

    const navigate = useNavigate();

    const handleChange = e => {
        if(e.target.name === 'email'){
        setInput({ ...input, email:e.target.value});
        } else if (e.target.name === 'password') {
        setInput({ ...input, password:e.target.value});
        console.log(e.target.name);
        } 
    }

    const onSubmit = async () => {
        try {
            const response = await axios.post("https://arhandev.maisyah.id/api/final/login", {
                email: input.email,
                password: input.password,
            });
            
            localStorage.setItem('token', response.data.data.token);
            localStorage.setItem('username', response.data.data.user.username);
            navigate("/");
            alert('berhasil');
        } catch (error) {
            console.log(error.response.data.info);
            // alert gagal
            alert('gagal')
        }
    }

    return (
        <div className='FormLogin'>
            <div className='cardLogin'>
                
                <div className="w-full max-w-sm p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700">
                    <form className="space-y-6" >
                        <h5 className="text-xl mb-4 font-medium text-gray-900 dark:text-white ">Login Form</h5>
                        <div className="mb-3 mt-3">
                            <label for="email" className="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Email</label>
                            <input type="email" name="email" id="email" onChange={handleChange} value={input.email} className="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="name@company.com" required/>
                        </div>
                        <div className="mb-3 mt-3">
                            <label for="password" className="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Password</label>
                            <input type="password" name="password" id="password" onChange={handleChange} value={input.password} placeholder="••••••••" class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required/>
                        </div>
                        <button type="submit" onClick={onSubmit} className="w-full mb-3 mt-4 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Login to your account</button>
                        <div class="text-sm  mb-3 mt-3 font-medium text-gray-500 dark:text-gray-300">
                            Not registered? <a href="/register" className="text-blue-700 hover:underline dark:text-blue-500">Create account</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    )
}

export default Login