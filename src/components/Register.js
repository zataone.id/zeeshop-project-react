import axios from 'axios';
import React from 'react'
import {useState} from 'react';
import { useNavigate } from 'react-router-dom';

function Register() {
    const [daftar, setDaftar] = useState({
        email: '',
        name: '',
        username: '',
        password: '',
        password_confirmation: ''
    });

    const navigate = useNavigate();

    const handleChange = e => {
        if(e.target.name === 'email'){
        setDaftar({ ...daftar, email:e.target.value});
        } else if (e.target.name === 'name') {
        setDaftar({ ...daftar, name:e.target.value});
        } else if (e.target.name === 'username') {
        setDaftar({ ...daftar, username:e.target.value});
        } else if (e.target.name === 'password') {
        setDaftar({ ...daftar, password:e.target.value});
        } else if (e.target.name === 'password_confirmation') {
        setDaftar({ ...daftar, password_confirmation:e.target.value});
        } 
    }

    const onSubmit = async () => {
        try {
            const response = await axios.post("https://arhandev.maisyah.id/api/final/register", {
                email: daftar.email,
                name: daftar.name,
                username: daftar.username,
                password: daftar.password,
                password_confirmation: daftar.password_confirmation
            });
            navigate("/login");
            alert(response);
        } catch (error) {
            console.log(error.response.data.info);
            // alert gagal
            alert(error);
        }
    }

    return (
    <div className='FormRegister'>
            <div className='cardRegister'>
                
                <div class="w-full max-w-sm p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700">
                    <form class="space-y-6" action="#">
                        <h5 class="text-xl mb-4 font-medium text-gray-900 dark:text-white ">Register Form</h5>
                        <div class="mb-3 mt-3">
                            <label for="name" class="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Nama</label>
                            <input type="text" name="name" id="name" onChange={handleChange} value={daftar.name} class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="Masukkan Nama" required/>
                        </div>
                        <div class="mb-3 mt-3">
                            <label for="email" class="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Email</label>
                            <input type="email" name="email" id="email" onChange={handleChange} value={daftar.email} class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="name@company.com" required/>
                        </div>
                        <div class="mb-3 mt-3">
                            <label for="username" class="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Username</label>
                            <input type="text" name="username" id="username" onChange={handleChange} value={daftar.username} class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="Masukkan Username" required/>
                        </div>
                        <div class="mb-3 mt-3">
                            <label for="password" class="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Password</label>
                            <input type="password" name="password" id="password" onChange={handleChange} value={daftar.password} placeholder="••••••••" class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required/>
                        </div>
                        <div class="mb-3 mt-3">
                            <label for="password_confirmation" class="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Konfirmasi password</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" onChange={handleChange} value={daftar.password_confirmation} placeholder="••••••••" class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required/>
                        </div>
                        
                        <button type="submit" onClick={onSubmit} class="w-full mb-3 mt-3 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Sign up</button>
                        
                    </form>
                </div>

            </div>
        </div>
  )
}

export default Register