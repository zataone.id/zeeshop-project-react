import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'


function TableSetting() {
    const [products, setProduct] = useState([]);

    const fetchProduct = async () => {
    try {
        const response = await axios.get(
            "https://arhandev.maisyah.id/api/final/products"
        );
        setProduct(response.data.data);
        } catch (error) {
        console.log(error);
        }
    };

    const deleteProduct = async (produkId) => {
    try {
        await axios.delete(
            `https://arhandev.maisyah.id/api/final/products/${produkId}`
        );
        fetchProduct();
        alert("Berhasil menghapus data");
        } catch (error) {
        console.log(error);
            alert("Gagal menghapus data");
        }
    };

    useEffect(() => {
        fetchProduct();
    }, []);

    return (
        <div className="listData">
            <div className="HeaderSetting">
                <h1 className="nameHeader">Setting Products</h1>
                <a href="/create">
                    <button class="px-4 py-2 ">Add product</button>
                </a>
            </div>
            <div className="Filtering flex justify-end flex-row">
                <div className='filterkategory flex flex-col'>
                    <button id="dropdownDefaultButton" data-dropdown-toggle="dropdown" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" type="button">Dropdown button <svg class="w-4 h-4 ml-2" aria-hidden="true" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path></svg></button>
                    
                    <div id="dropdown" class="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700">
                        <ul class="py-2 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdownDefaultButton">
                        <li>
                            <a href="#" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Dashboard</a>
                        </li>
                        <li>
                            <a href="#" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Settings</a>
                        </li>
                        <li>
                            <a href="#" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Earnings</a>
                        </li>
                        <li>
                            <a href="#" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Sign out</a>
                        </li>
                        </ul>
                    </div>
                </div>
                <div className=' filterSearch flex  items-center'>
                    <label for="simple-search" class="sr-only">Search</label>
                    <div class="relative w-40">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                        </div>
                        <input type="text" id="simple-search" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search" required/>
                    </div>
                    <button type="submit" class="p-2.5 ml-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                        <span class="sr-only">Search</span>
                    </button>
                </div>
            </div>
            <div className='TableProducts'>
                <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    Nama Barang
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Harga
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Harga Diskon
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Image
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Stock
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Category
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Dibuat Oleh
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Dibuat Pada
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.map((produk,index) => [
                            <tr  key={index} class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <td class="px-6 py-4 thname font-medium text-gray-900 dark:text-white">
                                    {produk.nama}
                                </td>
                                <td class="px-6 py-4">
                                    Rp.{produk.harga_display}
                                </td>
                                <td class="px-6 py-4">
                                    Rp.{produk.harga_diskon_display}
                                </td>
                                <td class="px-1 py-2">
                                    <img src={produk.image_url} alt="product" className="imageProduct" />
                                </td>
                                <td class="px-6 py-4">
                                    {produk.stock}
                                </td>
                                <td class="px-6 py-4">
                                    {produk.category}
                                </td>
                                <td class="px-6 py-4">
                                    {produk.user.name}
                                </td>
                                <td class="px-6 py-4">
                                    {produk.created_at}
                                </td>
                                <td class="px-6 py-4 text-right">
                                    <button type="button" class="focus:outline-none text-white bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:focus:ring-yellow-900"><Link to={`/update/${produk.id}`}>Edit</Link></button>
                                    <button type="button" onClick={() => deleteProduct(produk.id)} class="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Delete</button>
                                
                                </td>
                            </tr>
                            ])}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default TableSetting