import axios from 'axios';
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';

function CreateForm() {
    const [checked, setChecked] = useState(true);

    const [create, setCreate] = useState({
        nama: '',
        harga:'',
        is_diskon:'',
        harga_diskon:'',
        stock:'',
        deskripsi:'',
        category:'',
        image_url:'',
    });
    

    const navigate = useNavigate();

    const handleChange = e => {
        if(e.target.name === 'nama'){
        setCreate({ ...create, nama:e.target.value});
        } else if (e.target.name === 'harga') {
        setCreate({ ...create, harga:e.target.value});
        } else if (e.target.name === 'is_diskon') {
        setChecked(!checked);
        setCreate({ ...create, is_diskon:checked});
        } else if (e.target.name === 'harga_diskon') {
        setCreate({ ...create, harga_diskon:e.target.value});
        } else if (e.target.name === 'stock') {
        setCreate({ ...create, stock:e.target.value});
        }  else if (e.target.name === 'deskripsi') {
        setCreate({ ...create, deskripsi:e.target.value});
        }  else if (e.target.name === 'category') {
        setCreate({ ...create, category:e.target.value});
        }  else if (e.target.name === 'image_url') {
        setCreate({ ...create, image_url:e.target.value});
        } 
    }

    const onSubmit = async () => {
        try {
            const response = await axios.post("https://arhandev.maisyah.id/api/final/products", {
                nama: create.nama,
                is_diskon: checked,
                harga: create.harga,
                harga_diskon: create.harga_diskon,
                stock: create.stock,
                description: create.description,
                category: create.category,
                image_url: create.image_url
            });
            navigate("/setting");
        } catch (error) {
            console.log(error.response.data.info);
            // alert gagal
        }
    }

    return (
        <div className='FormLogin'>
            <div className='cardLogin'>
                
                <div class="w-full max-w-sm p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700">
                    <form class="space-y-6" >
                        <h5 class="text-xl mb-4 font-medium text-gray-900 dark:text-white ">Create Product Form</h5>
                        <div class="mb-3 mt-3">
                            <label for="nama" class="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Nama</label>
                            <input type="text" name="nama" id="nama" onChange={handleChange} value={create.nama} class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="name@company.com" required/>
                        </div>
                        <div class="mb-3 mt-3">
                            <label for="harga" class="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Harga</label>
                            <input type="text" name="harga" id="harga" onChange={handleChange} value={create.harga} placeholder="••••••••" class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required/>
                        </div>
                        <div class="flex items-start mb-3 mt-3">
                            <div class="flex items-start flex-col">
                                <label for="remember" class=" mb-4 text-sm font-medium text-gray-900 dark:text-gray-300">Is Diskon</label>
                                <div class="flex items-center h-5">
                                    <input id="remember" type="checkbox" value={checked} class="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800" required/>
                                </div>
                            </div>
                        </div>
                        { 
                            checked === true ? 
                            <div class="mb-3 mt-3 text-left" style={{display:'none'}}>
                            <label for="deskripsi" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Deskripsi</label>
                            <textarea id="deskripsi" onChange={handleChange} value={create.deskripsi} rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Write your thoughts here..."></textarea>
                        </div> : <div class="mb-3 mt-3 text-left" style={{display:'block'}}>
                            <label for="deskripsi" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Deskripsi</label>
                            <textarea id="deskripsi" onChange={handleChange} value={create.deskripsi} rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Write your thoughts here..."></textarea>
                        </div>
                        }
                        <div class="mb-3 mt-3 text-left">
                            <label for="deskripsi" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Deskripsi</label>
                            <textarea id="deskripsi" onChange={handleChange} value={create.deskripsi} rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Write your thoughts here..."></textarea>
                        </div>
                        <div class="mb-3 mt-3 text-left">
                            <label for="countries" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kategori Product</label>
                            <select id="countries" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option selected>Filter Kategori</option>
                                <option value="teknologi">Teknologi</option>
                                <option value="makanan">Makanan</option>
                                <option value="minuman">Minuman</option>
                                <option value="lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="mb-3 mt-3">
                            <label for="image" class="block mb-2 text-sm text-left font-medium text-gray-900 dark:text-white">Image Url</label>
                            <input type="text" name="image" id="image" onChange={handleChange} value={create.image_url} placeholder="Masukkan Image Url" class="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required/>
                        </div>
                        <button type="submit" onClick={onSubmit} class="w-full mb-3 mt-3 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    )
}

export default CreateForm