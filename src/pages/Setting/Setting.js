import React from 'react'
import Navbar from "../../components/Navbar";
import TableSetting from "../../components/TableSetting";

function Setting() {
    return (
        <>
        
            <Navbar />
        
            <TableSetting />
        
        </>
    )
}

export default Setting