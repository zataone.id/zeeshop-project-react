import React from 'react';
import Navbar from "../../components/Navbar";
import HomeWeb from "../../components/Home";

function Home() {
    return (
        <>
        
            <Navbar />
        
            <HomeWeb />
        
        </>
        
    )
}


export default Home
