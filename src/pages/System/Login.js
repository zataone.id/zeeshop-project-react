import React from 'react'
import Navbar from "../../components/Navbar";
import FormLogin from "../../components/Login";

function Login() {
    return (
        <>
        
            <Navbar />
        
            <FormLogin />
        
        </>
    )
}

export default Login