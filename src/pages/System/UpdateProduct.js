import React from 'react'
import Navbar from "../../components/Navbar";
import UpdateForm from "../../components/UpdateForm";

function UpdateProduct() {
    return (
        <>
            
            <Navbar />
        
            <UpdateForm />
        
        </>
    )
}

export default UpdateProduct