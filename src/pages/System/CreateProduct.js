import React from 'react'
import Navbar from "../../components/Navbar";
import CreateForm from "../../components/CreateForm";

function CreateProduct() {
    return (
        <>
            
                <Navbar />
            
                <CreateForm />
            
            </>
    )
}

export default CreateProduct