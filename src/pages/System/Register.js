import React from 'react'
import Navbar from "../../components/Navbar";
import FormRegister from "../../components/Register";

function Register() {
    return (
        <>
        
            <Navbar />
        
            <FormRegister />
        
        </>
    )
}

export default Register