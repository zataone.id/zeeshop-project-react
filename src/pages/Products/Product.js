import {useEffect, useState} from 'react'
import axios from "axios";
import Navbar from "../../components/Navbar";
import { Link } from 'react-router-dom';

function Product() {
    const [products, setProduct] = useState([]);

    const fetchProduct = async () => {
    try {
        const response = await axios.get(
            "https://arhandev.maisyah.id/api/final/products"
        );
        setProduct(response.data.data);
        } catch (error) {
        console.log(error);
        }
    };

    useEffect(() => {
        fetchProduct();
    }, []);

    return (
        <><Navbar /><div>
            <div class="HeaderList">
                <h1 class="nameHeader">All Product</h1>
            </div>
            <div className="containerList">
                <div className="dataProduct">
                    {products.map((produk) => [
                        <div className="card">
                            <Link to={`/detail/${produk.id}`}>
                            <div className="card2">
                                <img src={produk.image_url} alt="product" className="product" />
                                <div className="List-data">
                                    <h1>{produk.nama.substring(0, 20)}</h1>
                                    {produk.is_diskon === 1 ? <div className='diskon'> <p className="harga-asli">Rp.{produk.harga_display}</p> <p className="harga-diskon">Rp.{produk.harga_diskon_display}</p></div>
                                        : produk.is_diskon === 0 ? <p className='hargaAsli'>Rp.{produk.harga_display}</p>
                                            : "gagal"}
                                    {/* <p>Rp.{produk.harga}</p> */}
                                    <p className="stock">Stock {produk.stock}</p>
                                </div>
                            </div>
                            </Link>
                        </div>
                    ])}
                </div>
            </div>
        </div></>
    )
}

export default Product