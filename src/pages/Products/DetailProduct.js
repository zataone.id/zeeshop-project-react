import {useEffect, useState} from 'react'
import axios from "axios";
import Navbar from "../../components/Navbar";
import { useParams } from 'react-router-dom';




function DetailProduct() {
    const { id } = useParams();
    const [products, setProduct] = useState([]);

    const fetchProduct = async () => {
    try {
        const response = await axios.get(
            `https://arhandev.maisyah.id/api/final/products/${id}`
        );
        setProduct(response.data.data);
        } catch (error) {
        console.log(error);
        }
    };

    useEffect(() => {
        fetchProduct();
    }, []);

    return (
        <><Navbar />
        <div className='detailProduct'>
            <div className='cardDetail'>
                
                <div class="flex flex-col  bg-white border border-gray-200 rounded-lg shadow md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
                    <img class="object-cover w-full rounded-t-lg h-96 md:h-auto md:w-48 md:rounded-none md:rounded-l-lg " src={products.image_url} alt=""/>
                    <div class="px-5 pb-5 flex flex-col max-w-lg">
                        <div className="flex text-left p-1 mt-3">
                            <h5 class="text-xl font-bold tracking-tight text-gray-900 dark:text-white">{products.nama}</h5>
                        </div>
                        <h5 class="text-lg text-left font-semibold  tracking-tight p-1 mt-2">Kategori</h5>
                        <div className="flex text-left p-1">
                            <h5 class="text-base tracking-tight">{products.category}</h5>
                        </div>
                        <h5 class="text-lg text-left font-semibold tracking-tight p-1 mt-2 ">Deskripsi</h5>
                        <div className="flex text-left p-1 mb-5 ">
                            <p>{products.description}</p>
                        </div>
                        <div class="flex items-center justify-between p-1 mt-3">
                            {products.is_diskon === 1 ? <div className='diskon py-5'> <p className="harga-asli">Rp.{products.harga_display}</p> <p className="harga-diskon">Rp.{products.harga_diskon_display}</p></div>
                                        : <p className='hargaAsli py-5'>Rp.{products.harga_display}</p>}
                            <a href="/" class="  text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Beli</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default DetailProduct